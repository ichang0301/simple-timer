var timer1; //タイマーを格納する変数（タイマーID）の宣言

//カウントダウン関数を1000ミリ秒毎に呼び出す関数
function cntStart() {
  document.timer.elements[2].disabled = true;
  timer1 = setInterval("countDown()", 1000);
}

//カウントダウン後はカウントアップする関数
function cntUpStart() {
  timeOverDesign();
  timer2 = setInterval("countUp()", 1000);
}

//タイマー停止関数
function cntStop() {
  document.timer.elements[2].disabled = false;
  clearInterval(timer1);
  clearInterval(timer2);
}

//カウントダウン関数
function countDown() {
  var min = document.timer.elements[0].value;
  var sec = document.timer.elements[1].value;

  if (min == "" && sec == "") {
    alert("時刻を設定してください！");
    reSet();
  } else {
    if (min == "") min = 0;
    min = parseInt(min);

    if (sec == "") sec = 0;
    sec = parseInt(sec);

    tmWrite(min * 60 + sec - 1);
  }
}

//カウントアップ関数
function countUp() {
  var min = document.timer.elements[0].value;
  var sec = document.timer.elements[1].value;

  if (min == "") min = 0;
  min = parseInt(min);

  if (sec == "") sec = 0;
  sec = parseInt(sec);

  tmWrite(min * 60 + sec + 1);
}

//残り時間を書き出す関数
function tmWrite(int) {
  int = parseInt(int);

  if (int < 0) {
    clearInterval(timer1);
    cntUpStart();
  } else {
    //残り分数はintを60で割って切り捨てる
    minInt = Math.floor(int / 60);
    minString = minInt.toString();
    if (minInt < 10) {
      minString = "0" + minString;
    }
    document.timer.elements[0].value = minString;
    //残り秒数はintを60で割った余り
    secInt = int % 60;
    secString = secInt.toString();
    if (secInt < 10) {
      secString = "0" + secString;
    }
    document.timer.elements[1].value = secString;
  }
}

//フォームを初期状態に戻す（リセット）関数
function reSet() {
  timeInDesign();
  document.timer.elements[0].value = "";
  document.timer.elements[1].value = "";
  document.timer.elements[2].disabled = false;
  clearInterval(timer1);
  clearInterval(timer2);
}

//時間内の時は、背景色を白に、文字色は黒に
function timeInDesign() {
  document.timer.elements[0].style.background = "white";
  document.timer.elements[1].style.background = "white";
  document.timer.elements[0].style.color = "black";
  document.timer.elements[1].style.color = "black";
}

//時間がオーバーした時は、背景色を赤に、文字色は白に
function timeOverDesign() {
  document.timer.elements[0].style.background = "red";
  document.timer.elements[1].style.background = "red";
  document.timer.elements[0].style.color = "white";
  document.timer.elements[1].style.color = "white";
}

//ベルを鳴らす
function startWorkerForRingOnce() {
  var bell1 = new Audio("sounds/bell1.mp3");
  bell1.play();
}
